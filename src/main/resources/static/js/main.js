var app = angular.module('project', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider
	    .when('/plants', {
	      controller:'PlantsController',
	      templateUrl:'views/plants/list.html'
	    })
	    .when('/phrs', {
	      controller: 'PHRController',
	      templateUrl: 'views/phrs/list.html'
	    })
	    .when('/phrs/create', {
	      controller: 'CreatePHRController',
	      templateUrl: 'views/phrs/create.html'
	    })
	    .otherwise({
	      redirectTo:'/phrs'
	    });
	})