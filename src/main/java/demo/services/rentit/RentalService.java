package demo.services.rentit;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.services.PlantNotAvailableException;

@Service
public class RentalService {
	@Autowired
	RestTemplate restTemplate;
	
	public List<PlantResource> findAvailablePlants(String plantName, Date startDate, Date endDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		PlantResource[] plants = restTemplate.getForObject(
				"http://rentit.com/rest/plants?name={name}&startDate={start}&endDate={end}", 
				PlantResource[].class, 
				plantName,
				formatter.format(startDate),
				formatter.format(endDate));
		return Arrays.asList(plants);
	}
	
	public PurchaseOrderResource createPurchaseOrder(PlantResource plant, Date startDate, Date endDate) throws RestClientException, PlantNotAvailableException {
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setPlant(plant);
		po.setStartDate(startDate);
		po.setEndDate(endDate);
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			CloseableHttpClient client = HttpClientBuilder.create().build();
			HttpPost request = new HttpPost("http://localhost:9000/rest/pos");
			
			request.setEntity(new StringEntity(mapper.writeValueAsString(po), ContentType.APPLICATION_JSON));
			HttpResponse response = client.execute(request);
			PurchaseOrderResource pos = mapper.readValue(response.getEntity().getContent(), PurchaseOrderResource.class);
			
			client.close();
			return pos;
		} catch (IOException e) {
			throw new RestClientException(e.getMessage());
		}		
	}
}
