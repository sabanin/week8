package demo.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
public class PlantHireRequest {	
	@Id
	@GeneratedValue
	Long id;
	
	String plantRef;
    String plantName;

	String purchaseOrderRef;
	
	@OneToOne
	SiteEngineer siteEngineer;
	@OneToOne
	WorkEngineer workEngineer;
	
	@Temporal(TemporalType.DATE)
	Date startDate;
	
	@Temporal(TemporalType.DATE)
	Date endDate;
	
	Float price;
	
	@Enumerated(EnumType.STRING)
	PHRStatus status;
}
