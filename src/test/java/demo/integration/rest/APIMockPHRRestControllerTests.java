package demo.integration.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import demo.Application;
import demo.SimpleDbConfig;
import demo.integration.dto.PlantHireRequestResource;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, SimpleDbConfig.class})
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
public class APIMockPHRRestControllerTests {
    @Autowired
    private WebApplicationContext wac;
    
    private MockMvc mockMvc;
    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
	@Test
	@DatabaseSetup(value="DatabaseWithApprovedPHR.xml")
	public void testApprovePlantHireRequest() throws Exception {
		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andExpect(status().isOk())
				.andReturn();
		PlantHireRequestResource phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestResource.class);
		assertThat(phrp.getLink("submitPurchaseOrder"), is(notNullValue()));

		Link submit = phrp.getLink("submitPurchaseOrder");
		
		result = mockMvc.perform(post(submit.getHref()))
				.andExpect(status().isCreated())
				.andReturn();
	}
	
}
