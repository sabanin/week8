FORMAT: 1A
HOST: http://localhost

# ESI14-RentIt
Excerpt of RentIt's API

# Group Purchase Orders
Notes related resources of the **Plants API**

## Purchase Order Management [/rest/pos]
### Retrieve Purchase Orders [GET]
+ Response 200 (application/json)

        [{
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/pos/10001" }
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14",
            "cost":750.0
        }]

### Create Purchase Order [POST]
+ Request (application/json)

        {
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14",
        }


+ Response 201 (application/json)

        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/pos/10001" }
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14",
            "cost":750.0
        }

+ Response 409 (application/json)

        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/pos/10001" }
            ],
            "_links":[
                { "rel":"updatePO", "href":"http://localhost:9000/rest/pos/10001", "method":"PUT" }
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ],
                "_links":[]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14"
        }

